-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: checkmp3
Binary: checkmp3
Architecture: any
Version: 1.98-10
Maintainer: Nicholas Breen <nbreen@ofb.net>
Dm-Upload-Allowed: yes
Homepage: http://sourceforge.net/projects/mp3check
Standards-Version: 3.9.2
Build-Depends: debhelper (>= 5.0.0)
Checksums-Sha1: 
 7cdeba02431b2a68a5c13ec986f812ff49f5ed38 30308 checkmp3_1.98.orig.tar.gz
 abae483c45525cb1f0d6a9aa541f98daa7e50079 8297 checkmp3_1.98-10.debian.tar.gz
Checksums-Sha256: 
 33536fe33e3c59cfdfa080e5842ee01bd9116223ac1653e4ea3655c1443546bd 30308 checkmp3_1.98.orig.tar.gz
 da0883822c88bb03e4924e3777cc8ac20451f2b0a7f49ba2bed180078f850f55 8297 checkmp3_1.98-10.debian.tar.gz
Files: 
 fa5c741e8614dd47e82df3d7ddab8405 30308 checkmp3_1.98.orig.tar.gz
 35e48ffb9db50ac3d109d4c07a1e33d0 8297 checkmp3_1.98-10.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.11 (GNU/Linux)

iQIVAwUBTl18s+5K/WnsZRCPAQitAg//ao70y6O4AWrVhnR8JmTm6U9FwLkRY3ez
TcH+C94qiw7lAfjmqSMY9Jn96HSmQ2onPa+wnKx9ro7hcyoY08pzOMKjQkH7QIVe
N7cEDFGEwO1UV1mhctQcS4XrRH5O6f4ON7B0a/doiuGiBlCGwSv7wtneSbqkLtXN
j0UTYne2Tztz7VxJgBNjqOTFnEbQHxsZnDSDavLhvrTW2FBjZ5GKlSF7ZHmMYVp2
L4llMZQSM9i/QKP/XieXxg+J+YmDMkHJN+LVqspwebYdifSE78lH0qX7McEtpSZW
ppgtLzIRvOUfxoJafmPelBbq5VgchTJZYGWrHZV1rh7lV34zmsLh8KxcswAy9NKr
7HupF1cCp3jsUQWeOzusGXCmb0cY9qmTgfNno6gNgINuhnbo8KrlSvyDAUKBZqbK
+Rivcnkc8U5UcN/EoeL0d/fmJKgOn/h7bD04cSfbS3LxH2vnItJUFdV2jvjJy3jY
JJZUGwFjR7hNSA9DergisDthO/3e19UGQpSgaGlDvQhARSsfrPuhpvQ59mrAbdHd
9JnhDWZP8U+x65VOTwht/3L9UawoVbeBqp4YMk89ZJyybTO0oRAHSIk8/ORMefnZ
E2zgDTLulFcbJvFUzBTG3T0Hgwad1E3rSO89qWet30MQY9pq9CCF68lencrHraLk
D7gAOpSG4jE=
=T5m2
-----END PGP SIGNATURE-----
