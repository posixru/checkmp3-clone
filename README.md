# checkmp3

checkmp3 (mp3_check) helps to identify in explicit detail MP3s that do not correctly follow the MP3 format.

It also looks for invalid frame headers, missing frames, etc., and generates useful statistics.  This can be useful when building a high-quality mp3 archive...

![img](http://zenway.ru/uploads/09_18/checkmp3-004.png)

Консольные утилиты для поиска и устранения ошибок в MP3 файлах.

http://zenway.ru/page/checkmp3